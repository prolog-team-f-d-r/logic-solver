from django.shortcuts import render
from prolog.adapter import PrologAdapter
from django.conf import settings

def landing(request):
    if request.method == 'GET':
        return render(request, 'index.html')

    elif request.method == 'POST':
        formula = request.POST.get('formula')
        prolog = PrologAdapter()
        result = list(prolog.solver(formula))
        with open(settings.PROLOG_SOLUTION, 'r') as f:
            steps = f.read()

        if len(result) > 0:
            context = {
                'formula': formula,
                'steps' : steps,
            }
        else:
            context = {
                'formula': formula,
                'steps' : 'Bukan theorem',
            }
        return render(request, 'index.html', context)
