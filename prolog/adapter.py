from django.conf import settings
from prolog.pyswipmt import PyswipMT

class PrologAdapter:
    def __init__(self):
        self.prolog = PyswipMT()
        prolog_filename = 'prolog/scripts/solver.pl'
        self.prolog.consult(prolog_filename)
        
    def solver(self, formula):
        query = "solver('{formula}', Result)".format(formula=formula) 
        return self.prolog.query(query)
