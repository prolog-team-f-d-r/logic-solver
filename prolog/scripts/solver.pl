:- use_module(library(codesio)).
:- consult(operator).
:- consult(cnf).
:- consult(resolution_prove).

solver(FIn, Res) :-
    tell('solution.txt'),
    write("---------Start-----------"), nl,
    read_from_codes(FIn, F),
    cnf(~F, CNF),
    term_string(CNF, CNF_STR),
    split_string(CNF_STR, '^', '', CNF_STR_LST),
    maplist(term_string, CNF_IN, CNF_STR_LST),
    addClausesToFact(CNF_IN, CNF2, 1), !,
    reverse(CNF2,CNF3),
    search(CNF3,P),
    reverse(P,Prove),
    write('Resolusi: '),
    nl,
    writeProve(Prove),
    term_to_atom(Prove, Res),
    nl, write(FIn), write(" is a theorem"),
    nl, write("----------End------------"),
    told,
    !.

writeProve([]).
writeProve([c(N,C,From)|P]) :-
    swritef(Clauseproof, '%3R. %20L %w', [N, C, From]),
    write(Clauseproof),
    nl,
    writeProve(P),
    !.
