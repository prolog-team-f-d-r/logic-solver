:- use_module(library(codesio)).

:- consult(operator).

rules('Material Implication', P => Q, ~P v Q).
rules('Material Biimplication', P <=> Q, (~P v Q) ^ (P v ~Q)).
rules('Double Negation', ~(~P), P).
rules('De Morgan', ~(P ^ Q), P v Q).
rules('De Morgan', ~(P v Q), P ^ Q).
rules('Distribution', (P ^ Q) v R, (P v R) ^ (Q v R)).
rules('Distribution', P v (Q ^ R), (P v Q) ^ (P v R)).

step('Eliminate (Bi)Implications', rules(Rule, P1 => Q1, ~P2 v Q2)) :-
    step('Eliminate (Bi)Implications', rules(Rule, P1, P2)),
    step('Eliminate (Bi)Implications', rules(Rule, Q1, Q2)),
    write('(Material Implication)  '),
    write(P1 => Q1),
    write(' === '),
    write(~P2 v Q2),
    nl,
    !.
step('Eliminate (Bi)Implications', rules(Rule, P1 <=> Q1, (~P2 v Q2) ^ (P2 v ~Q2))) :-
    step('Eliminate (Bi)Implications', rules(Rule, P1, P2)),
    step('Eliminate (Bi)Implications', rules(Rule, Q1, Q2)),
    write('(Material Biimplication)  '),
    write(P1 <=> Q1),
    write(' === '),
    write((~P2 v Q2) ^ (P2 v ~Q2)),
    !.
step('Push Negations', rules(Rule, ~(~P1), P2)) :-
    step('Push Negations', rules(Rule, P1, P2)),
    write('(Double Negation)  '),
    write(~(~P1)),
    write(' === '),
    write(P2),
    nl,
    !.
step('Push Negations', rules(Rule, ~(P1 ^ Q1), P2 v Q2)) :-
    step('Push Negations', rules(Rule, ~P1, P2)),
    step('Push Negations', rules(Rule, ~Q1, Q2)),
    write('(De Morgan)  '),
    write(~(P1 ^ Q1)),
    write(' === '),
    write(P2 v Q2),
    nl,
    !.
step('Push Negations', rules(Rule, ~(P1 v Q1), P2 ^ Q2)) :-
    step('Push Negations', rules(Rule, ~P1, P2)),
    step('Push Negations', rules(Rule, ~Q1, Q2)),
    write('(De Morgan)  '),
    write(~(P1 v Q1)),
    write(' === '),
    write(P2 ^ Q2),
    nl,
    !.

step('Push Disjunctions', rules(Rule, (P1 ^ Q1) v R1, (P2 v R2) ^ (Q2 v R2))) :-
    step('Push Disjunctions', rules(Rule, P1, P2)),
    step('Push Disjunctions', rules(Rule, Q1, Q2)),
    step('Push Disjunctions', rules(Rule, R1, R2)),
    write('(Distribution)  '),
    write((P1 ^ Q1) v R1),
    write(' === '),
    write((P2 v R2) ^ (Q2 v R2)),
    nl,
    !.
step('Push Disjunctions', rules(Rule, P1 v (Q1 ^ R1), (P2 v Q2) ^ (P2 v R2))) :-
    step('Push Disjunctions', rules(Rule, P1, P2)),
    step('Push Disjunctions', rules(Rule, Q1, Q2)),
    step('Push Disjunctions', rules(Rule, R1, R2)),
    write('(Distribution)  '),
    write(P1 v (Q1 ^ R1)),
    write(' === '),
    write((P2 v Q2) ^ (P2 v R2)),
    nl,
    !.

step(Step, rules(Rule, P1 ^ Q1, P2 ^ Q2)) :-
    step(Step, rules(Rule, P1, P2)),
    step(Step, rules(Rule, Q1, Q2)),
    !.
step(Step, rules(Rule, P1 v Q1, P2 v Q2)) :-
    step(Step, rules(Rule, P1, P2)),
    step(Step, rules(Rule, Q1, Q2)),
    !.
step(Step, rules(Rule, ~P1, ~P2)) :-
    step(Step, rules(Rule, P1, P2)),
    !.
step(_, rules(_, P, P)).

concatenate(StringList, StringResult) :-
    maplist(atom_chars, StringList, Lists),
    append(Lists, List),
    atom_chars(StringResult, List).

replace(CharTarget, CharReplace, StringIn, StringOut) :-
    split_string(StringIn, CharTarget, " ", List),
    atomics_to_string(List, CharReplace, StringOut).

fix_parentheses(FIn, FOut) :-
    term_string(FIn, FString),
    replace("()", " ", FString, FString1),
    replace("^", ") ^ (", FString1, FString2),
    concatenate(["(", FString2, ")"], FString3),
    term_string(FOut, FString3).

cnf(F, CNF) :-
    write('Original: '),
    F = ~F0,
    write(F0),
    nl,
    write('Negation: '),
    write(F),
    nl,
    nl,
    write('Proses CNF:'),
    nl,
    step('Eliminate (Bi)Implications', rules(_, F, F1)),
    step('Push Negations', rules(_, F1, F2)),
    step('Push Disjunctions', rules(_, F2, F3)),
    fix_parentheses(F3, CNF),
    nl,
    write('CNF: '),
    write(CNF),
    nl,
    nl.
