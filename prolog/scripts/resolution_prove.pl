addClausesToFact([], [], _).
addClausesToFact( [HSt|TSt], St, N ):-
    intoList(HSt, HStList),
    tautology(HStList),
    addClausesToFact(TSt,St,N).
addClausesToFact( [HSt|TSt], [c(N, StList, hipotesis)|St], N ):-
    intoList(HSt, HStList),
    sort(HStList, StList),
    NextN is N + 1,
    addClausesToFact(TSt, St, NextN).

intoList(X v Y, [X|L]):-
    intoList(Y, L).
intoList(X, [X]).



% finding the tautology of statements
tautology(C) :-
  member(P, C), member(~P, C).


% Same exact
search( [c(N, [], R)|Path], [c(N, [], R)|Path] ).
% path not exactly the same, need check deeper
search( Path, Sol) :-
   succ( Path, Suc),
   search( Suc, Sol).


succ(Path,[c(Number,[],(X1, X2))|Path]) :-
  member(c(X1, [A], _), Path),
  member(c(X2, [~A], _), Path),
  length(Path, Len), Number is Len + 1.

% removing redundant
succ(Path,[c(Number,R,(X1,X2))|Path]) :-
  member(c(X1, C1, _), Path),
  member(A, C1),
  member(c(X2, C2, _), Path),
  member(~A, C2),
  delete(C1, A, R1),
  delete(C2, ~A, R2),
  append(R1, R2, R3),
  sort(R3, R),
  \+ tautology(R),
  \+ member(c(_, R, _), Path),
  length(Path, Len), Number is Len + 1.
