# Theorem Prover

Mata Kuliah: **Prolog**

Kelompok: **team(f,d,r)**.

Anggota:
- Faisal Hanif (1806133830)
- Dhipta Raditya (1706028644)
- Razaqa Dhafin (1706039484)


## Petunjuk Instalasi


### Prasyarat:
- *Software* ini memerlukan Python beserta `pip`. Disarankan menggunakan *virtual environment* dalam menjalankannya.
- *Software* ini juga memerlukan SWI-Prolog. Pastikan `swipl` berada pada `PATH`.


### Instalasi:
- Pastikan Anda berada di *root directory* proyek ini, yang ditandai dengan adanya berkas `requirements.txt` dan `manage.py`.
- Unduh dan pasang *package* yang diperlukan.
  ```
  pip install -r requirements.txt
  ```
- Nyalakan *server*.
  ```
  python manage.py runserver
  ```

  Setelah ini, *server* dapat diakses di http://localhost:8000
